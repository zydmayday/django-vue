# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from django.shortcuts import render

import os, sys
import patoolib
from shutil import copyfile

center = "../datacenter"
station = "../workstation"

def show_center():
    res = []
    show__recursively(res, center)
    return {"resources": res}

def show_station():
    res = []
    show__recursively(res, station)
    return {"resources": res}

# 递归解压
def extract_recursively(src, dst):
    if src.endswith(".rar"):
        patoolib.extract_archive(src, outdir=dst)
        idx1 = src.rfind("/")
        idx2 = src.index(".rar")
        src = src[idx1:idx2]
        extract_recursively(dst + src, dst)
    elif os.path.isdir(src):
        for file in os.listdir(src):
            extract_recursively(src + "/" + file, src)

# 递归删除*.rar
def delete__recursively(dst):
    if dst.endswith(".rar"):
        os.remove(dst)
    else:
        new = dst + "/"
        if os.path.isdir(new):
            for file in os.listdir(new):
                delete__recursively(new + file)


def extract(src, dst):
    if not src.endswith(".rar") and not os.path.isdir(src):
        copyfile(src, dst + src[src.rfind("/"):])
        return
    # consturct the dst folder
    idx = src.rfind("/")
    if idx != -1:
        dst = dst + src[:idx].replace(center, "")
    print(dst)
    if not os.path.exists(dst):
        os.mkdir(dst)
    extract_recursively(src, dst)
    delete__recursively(dst)


# 递归显示文件夹层级
def show__recursively(res, target):
    if os.path.isdir(target):
        for file in os.listdir(target):
            new = target + "/" + file
            if not os.path.isdir(new):
                res.append(new)
            show__recursively(res, new)
