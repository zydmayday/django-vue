from django.urls import path

from . import views

urlpatterns = [
    path('test', views.test),
    path('data-center', views.show_center),
    path('work-station', views.show_station),
    path('extract-to', views.extract_to)
]