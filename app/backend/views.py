# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse

from django.shortcuts import render

from . import resources
import json

# Create your views here.
def test(req):
    return JsonResponse({'name': 'jiang'})

def show_center(req):
    return JsonResponse(resources.show_center())

def show_station(req):
    return JsonResponse(resources.show_station())

def extract_to(req):
    srcs = json.loads(req.body)
    print(srcs)
    for src in srcs:
        resources.extract(src, '../workstation')
    return JsonResponse({'body': srcs})
    