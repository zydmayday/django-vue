import os, sys
import patoolib
from shutil import copyfile

center = "../src"
station = "../dst"

for file in os.listdir("./"):
    print(file)
    # extract(file)

def list_all(folder):
    for file in os.listdir(folder):
        print(file)

# 递归解压
def extract_recursively(src, dst):
    if src.endswith(".rar"):
        patoolib.extract_archive(src, outdir=dst)
        idx1 = src.rfind("/")
        idx2 = src.index(".rar")
        src = src[idx1:idx2]
        extract_recursively(dst + src, dst)
    elif os.path.isdir(src):
        for file in os.listdir(src):
            extract_recursively(src + "/" + file, src)
    else:
        copyfile(src, dst + src[src.rfind("/"):])

# 递归删除*.rar
def delete__recursively(dst):
    if dst.endswith(".rar"):
        os.remove(dst)
    else:
        new = dst + "/"
        if os.path.isdir(new):
            for file in os.listdir(new):
                delete__recursively(new + file)


def extract(src, dst):
    # consturct the dst folder
    idx = src.rfind("/")
    if idx != -1:
        dst = dst + src[:idx].replace(center, "")
    print(dst)
    if not os.path.exists(dst):
        os.mkdir(dst)
    extract_recursively(src, dst)
    delete__recursively(dst)


# 递归显示文件夹层级
def show__recursively(target):
    if os.path.isdir(target):
        for file in os.listdir(target):
            new = target + "/" + file
            if not os.path.isdir(new):
                print(new)
            show__recursively(new)


if __name__ == '__main__':
    src = "../src/abc/1.txt"
    
    extract(src, station)
    # show__recursively(dst)


# test case
# 1: src = '2Hour.rar' dst='dst'
# 2: src = '1Day.rar' dst='dst'
# 3: src = '2Hour.rar' dst='../dst'
# 4: src = '1Day/0Hour/0Minute.rar' dst='dst'
# 5: src = '../src/2Hour.rar' dst='dst' ?

