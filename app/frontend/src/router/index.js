import Vue from 'vue'
import Router from 'vue-router'
import DataCenter from '@/components/DataCenter'
import WorkStation from '@/components/WorkStation'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/data-center',
      name: 'DataCenter',
      component: DataCenter
    },
    {
      path: '/work-station',
      name: 'WorkStation',
      component: WorkStation
    }
  ]
})
